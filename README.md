# repl-c-d

#### Feature:

Caddy2 as frontend http server with [Diffuse](https://github.com/icidasset/diffuse) as disquise.

Thanks to lxhao61 for rendering ["Caddy2 with trojan-go plugin"](https://github.com/lxhao61/integrated-examples/releases)

### Deploy:

Deploy Caddy-trojan-go on replit.
Change Language to "Bash",Choose "Import from GitHub".

<a href="https://gitlab.com/gityzlab/repl-c-d">
  <img alt="Run on Repl.it" src="https://replit.com/badge/github/github/gityzon" style="height: 40px; width: 190px;" />
</a>

#### For edu:

1. Creat a Bash-language project.

2. Copy these code to Replit's Shell and ←

   `git clone https://gitlab.com/gityzlab/repl-c-d && mv -b repl-c-d/* ./ && mv -b repl-c-d/.[^.]* ./ && rm -rf *~ && rm -rf repl-c-d`

3. After "Loading Nix environment..." is done, ▶RUN!!!

   

   If you use mobile phone,please set the UA of Browser as "Desktop" or maybe replit can not work.

# Environment

You need to add Secrets(System environment variables) like:

### key: "uuid"

### value: "your own uuid"

The secrets is private so you don't need to worry about leaking your data.

# Clients

You can find all clients from:
[**this page**](https://itlanyan.com/trojan-go-clients-download/)

# Usage

- type:"trojan-go"
- password:"uuid"
- host:"your replit url"
- port:443
- stream-network:ws
- ws-path:/
- tls:tls

# Sleep

May the service sleep,you need a web monitor like UptimeRobot to keep it work.
https://uptimerobot.com



# Good luck!
