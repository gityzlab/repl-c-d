#！请添加$uuid环境变量后运行
#download
##caddy
if [ ! -f "caddy" ];then
    curl -L https://github.com/lxhao61/integrated-examples/releases/latest/download/caddy-linux-amd64.tar.gz -o caddy.tar.gz
    tar -zxvf caddy.tar.gz
    rm -f LICENSE && rm -f README.md && rm -f sha256 && rm -f caddy.tar.gz
    chmod +x caddy
fi

##diffuse
if [ ! -f "/home/runner/${REPL_SLUG}/build/index.html" ];then
    curl -L https://github.com/icidasset/diffuse/releases/download/3.2.0/diffuse-web.tar.gz -o diffuse-web.tar.gz
    tar -zxvf diffuse-web.tar.gz
    rm -f diffuse-web.tar.gz
fi

#config

if [ $uuid ];then
    cat > Caddyfile <<EOF
{
	order trojan before route
	admin off
	auto_https off
	log {
		output discard #关闭日志文件输出
		level INFO
	}
	servers :80 {
		listener_wrappers {
			trojan #caddy-trojan插件应用必须配置
		}
	}
	trojan {
		caddy
		no_proxy
		users $uuid #修改为自己的密码。密码可多组，用空格隔开。
	}
}

:80 {
	trojan {
		connect_method
		websocket #开启WebSocket支持
	} #此部分配置为trojan-go的WebSocket应用，若删除就仅支持trojan应用。

		file_server {
			root /home/runner/${REPL_SLUG}/build #修改为自己存放的WEB文件路径
		}
}
EOF
fi

#run
./caddy run --config /home/runner/${REPL_SLUG}/Caddyfile --adapter caddyfile
